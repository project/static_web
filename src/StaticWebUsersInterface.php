<?php

declare(strict_types=1);

namespace Drupal\static_web;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a static web users entity type.
 */
interface StaticWebUsersInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
