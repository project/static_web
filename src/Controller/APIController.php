<?php

namespace Drupal\static_web\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Serve as api call.
 */
class APIController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function jsonMain($uuid) {
    $json = [];
    $data = \Drupal::service('static_web.generator');
    $data = $data->setUuid($uuid);
    $json['main'] = $data->main();
    $json['header'] = $data->header();
    $json['footer'] = $data->footer();
    $json['sidebar'] = $data->sidebar();
    $json['sidebar_flag'] = $data->sidebarFlag();
    return new JsonResponse($json);
  }

  /**
   * {@inheritdoc}
   */
  public function getHomepageByDomainId($domain_id) {
    if ($domain_id && ctype_alnum($domain_id)) {
      $entity = \Drupal::entityTypeManager()
        ->getStorage('static_web_static_web_users')
        ->loadByProperties(['field_ws_site_name' => $domain_id]);
      if (empty($entity)) {
        return new JsonResponse(['status' => FALSE]);
      }
      return new JsonResponse(['status' => TRUE, 'uuid' => reset($entity)->field_ws_home_page->entity->uuid()]);
    }
    return new JsonResponse(['status' => FALSE]);
  }

  /**
   * {@inheritdoc}
   */
  public function setHomepageByNid($nid) {
    if (empty($nid)) {
      return new RedirectResponse('/admin/config/system/web-static-settings');
    }
    $uid = \Drupal::currenUser()->id();
    $entity = \Drupal::entityTypeManager()
      ->getStorage('static_web_static_web_users')
      ->loadByProperties(['field_ws_client' => $uid]);
    if (empty($entity)) {
      return new RedirectResponse('/admin/config/system/web-static-settings');
    }
    $entity = reset($entity);
    $entity->set('field_ws_home_page', $nid)->save();
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    \Drupal::messenger()->addMessage($node->getTitle() . ' was set to Homepage');
    return new RedirectResponse('/admin/config/system/web-static-settings');
  }

}
