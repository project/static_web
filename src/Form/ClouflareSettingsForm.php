<?php

declare(strict_types=1);

namespace Drupal\static_web\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Configure Admin Toolbar settings for this site.
 */
final class ClouflareSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'static_web_web_static_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['static_web.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $status_label = 'Disconnected';
    $status_class = 'form-color-red';
    if ($this->config('static_web.settings')->get('project_domain')) {
      $status_label = 'Connected';
      $status_class = 'form-color-green';
    }
    $form['project'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project Name'),
      '#default_value' => $this->config('static_web.settings')->get('project'),
      '#prefix' => '<div class="' . $status_class . '">' . $status_label . '</div>',
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloudflare Email'),
      '#default_value' => $this->config('static_web.settings')->get('email'),
      '#required' => TRUE,
    ];

    $form['account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#default_value' => $this->config('static_web.settings')->get('account'),
      '#required' => TRUE,
    ];

    $form['zone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zone ID'),
      '#default_value' => $this->config('static_web.settings')->get('zone'),
      '#required' => TRUE,
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#default_value' => $this->config('static_web.settings')->get('token'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkStatus(): bool {
    $token = $this->config('static_web.settings')->get('token');
    $email = $this->config('static_web.settings')->get('email');
    $project = $this->config('static_web.settings')->get('project');
    $account_id = $this->config('static_web.settings')->get('account');
    $zone_id = $this->config('static_web.settings')->get('zone');

    $headers = [
      'X-Auth-Email' => $email,
      'X-Auth-Key' => $token,
    ];
    try {
      $res = \Drupal::httpClient()->request('GET', 'https://api.cloudflare.com/client/v4/accounts/' . $account_id . '/pages/projects/' . $project . '/domains', ['headers' => $headers]);
      $contents = $res->getBody()->getContents();
      if (json_decode($contents)->success) {
        return TRUE;
      }
      return FALSE;
    }
    catch (ClientException $e) {
      return FALSE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSubDomain(&$form_state) {
    $data = [];
    $data['project'] = $form_state->getValue('project');
    $data['email'] = $form_state->getValue('email');
    $data['account_id'] = $form_state->getValue('account');
    $data['zone_id'] = $form_state->getValue('zone');
    $data['token'] = $form_state->getValue('token');
    return \Drupal::service('static_web.domain_handler')->setData($data)->getProjectDomain();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $project_domain = $form_state->getValue('project_domain');
    // Check if domain project was registered & don't request to cf anaymore.
    if (empty($form_state->getValue('project_domain'))) {
      $project_domain = $this->getSubDomain($form_state);
    }
    // If project name is changed, request new project domain to cf.
    if ($form_state->getValue('project') !==
      $this->config('static_web.settings')->get('project')) {
      $project_domain = $this->getSubDomain($form_state);
    }
    $this->config('static_web.settings')
      ->set('project', $form_state->getValue('project'))
      ->set('email', $form_state->getValue('email'))
      ->set('account', $form_state->getValue('account'))
      ->set('zone', $form_state->getValue('zone'))
      ->set('token', $form_state->getValue('token'))
      ->set('project_domain', $project_domain)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
