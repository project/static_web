<?php

declare(strict_types=1);

namespace Drupal\static_web\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure WebStaticSettingsForm settings for this site.
 */
final class WebStaticSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'static_web_web_static_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['static_web.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $uid = \Drupal::currentUser()->id();
    $entity = \Drupal::entityTypeManager()
      ->getStorage('static_web_static_web_users')
      ->loadByProperties(['field_ws_client' => $uid]);
    $entity = !empty($entity) ? reset($entity) : FALSE;

    $domain_link = $entity ? $entity->field_ws_domain_link->value : FALSE;
    $prefix = '';
    if ($domain_link) {
      $domain_link = '<div>
        <div>Youre Site:</div>
        <a href="https://' . $entity->field_ws_domain_link->value . '"
        target="_blank">' . $entity->field_ws_domain_link->value . '</a>
        </div>';
    }

    $form['site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain ID'),
      '#default_value' => $entity ? $entity->label() : FALSE,
      '#disabled' => $entity &&
      $entity->field_ws_site_name->value ? TRUE : FALSE,
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      '#description' => $this->t('Lower case only. Example mypage') .
      $domain_link,
      '#required' => TRUE,
    ];

    $form['entity'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
      '#attributes' => [
        'id' => 'entity_ws_container',
      ],
    ];

    if ($entity) {
      $webStaticClientForm = \Drupal::service('entity.form_builder')->getForm($entity, 'edit');
      $plain = \Drupal::service('renderer')->render($webStaticClientForm);
      $form['entity']['field_entity'] = [
        '#type' => 'markup',
        '#markup' => $plain,
      ];
    }

    $form['pages'] = [
      '#type' => 'details',
      '#title' => $this->t('My Pages'),
      '#open' => TRUE,
      '#attributes' => [
        'id' => 'page_container',
      ],
    ];
    $form['pages']['table'] = $this->buildTable();
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderForm'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getUserPages() {
    $uid = \Drupal::currentUser()->id();
    return \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['uid' => $uid]);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildTable() {
    $entities = $this->getUserPages();
    $rows = [];
    foreach ($entities as $entity) {
      $rows[$entity->getTitle()] = [
        [
          'data' =>
          new FormattableMarkup('<a href=":link">@name</a>',
          [
            ':link' => '/node/' . $entity->id(),
            '@name' => $entity->getTitle(),
          ]),
        ],
        ['data' => $this->checkCurrentHomePage($entity->id())],
        [
          'data' =>
          new FormattableMarkup('<a href=":link" class="button">@name</a>',
          [
            ':link' => '/admin/structure/static-web-users/default-homepage/' .
            $entity->id(),
            '@name' => 'Set Homepage',
          ]),
        ],
        [
          'data' =>
          new FormattableMarkup('<a href=":link">@name</a>',
          [
            ':link' => '/node/' . $entity->id() . '/edit',
            '@name' => 'Edit',
          ]),
        ],
      ];
    }

    return [
      '#type' => 'table',
      '#header' => [
        ['data' => 'Title'],
        ['data' => 'Is Homepage ?'],
        ['data' => 'Operation'],
        ['data' => 'Edit'],
      ],
      '#rows' => $rows,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCurrentHomePage($nid) {
    $uid = \Drupal::currentUser()->id();
    $entity = \Drupal::entityTypeManager()
      ->getStorage('static_web_static_web_users')
      ->loadByProperties(['field_ws_client' => $uid]);
    if (empty($entity)) {
      return FALSE;
    }
    $entity = reset($entity);
    $homepage_nid = $entity->field_ws_home_page->target_id;
    return $nid === $homepage_nid ? $this->t('Yes') : $this->t('No');
  }

  /**
   * {@inheritdoc}
   * */
  protected function generateDomain($site_name) {
    $data['site_name'] = $site_name;
    $data['account_id'] = $this->config('static_web.settings')->get('account');
    $data['email'] = $this->config('static_web.settings')->get('email');
    $data['token'] = $this->config('static_web.settings')->get('token');
    $data['project'] = $this->config('static_web.settings')->get('project');
    $data['project_domain'] = $this->config('static_web.settings')->get('project_domain');
    $data['zone_id'] = $this->config('static_web.settings')->get('zone');
    return \Drupal::service('static_web.domain_handler')->setData($data)->generateDomain();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uid = \Drupal::currentUser()->id();
    $site = $form_state->getValues()['site'];
    $entity = \Drupal::entityTypeManager()
      ->getStorage('static_web_static_web_users')
      ->loadByProperties(['field_ws_site_name' => $site]);
    if (!empty($entity) && $uid !== reset($entity)->field_ws_client->target_id) {
      return $form_state->setErrorByName('site', $this->t('Domain ID name already used'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $uid = \Drupal::currentUser()->id();
    $entity = \Drupal::entityTypeManager()
      ->getStorage('static_web_static_web_users')
      ->loadByProperties(['field_ws_client' => $uid]);

    if (empty($entity)) {
      $response = $this->generateDomain($form_state->getValue('site'));
      if (empty($response)) {
        \Drupal::messenger()->addError('Domain ID is not valid');
      }
      else {
        \Drupal::entityTypeManager()
          ->getStorage('static_web_static_web_users')
          ->create([
            'label' => $form_state->getValue('site'),
            'field_ws_site_name' => strtolower($form_state->getValue('site')),
            'field_ws_client' => $uid,
            'field_ws_domain_uuid' => $response->result->domain_id,
            'field_ws_domain_page_id' => $response->result->id,
            'field_ws_domain_link' => $response->result->name,
          ])->save();
      }
    }
    else {
      $entity = reset($entity);
      // Regenerate new domain if changed.
      if ($form_state->getValue('site') !== $entity->field_ws_site_name->value) {
        $response = $this->generateDomain($form_state->getValue('site'));
        $entity->set('field_ws_domain_uuid', $response->result->domain_id);
        $entity->set('field_ws_domain_page_id', $response->result->id);
        $entity->set('field_ws_domain_link', $response->result->name);
      }
      $entity->set('label', $form_state->getValue('site'));
      $entity->set('field_ws_site_name', $this->trimString($form_state->getValue('site')));
      $entity->save();
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function trimString($text) {
    $text = preg_replace('/\s+/', '', $text);
    return strtolower($text);
  }

}
