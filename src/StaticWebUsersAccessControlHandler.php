<?php

declare(strict_types=1);

namespace Drupal\static_web;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the static web users entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class StaticWebUsersAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view static_web_static_web_users', 'administer static_web_static_web_users'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit static_web_static_web_users', 'administer static_web_static_web_users'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete static_web_static_web_users', 'administer static_web_static_web_users'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create static_web_static_web_users', 'administer static_web_static_web_users'], 'OR');
  }

}
