<?php

declare(strict_types=1);

namespace Drupal\static_web\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\static_web\StaticWebUsersInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the static web users entity class.
 *
 * @ContentEntityType(
 *   id = "static_web_static_web_users",
 *   label = @Translation("Static Web Users"),
 *   label_collection = @Translation("Static Web Users"),
 *   label_singular = @Translation("static web users"),
 *   label_plural = @Translation("static web users"),
 *   label_count = @PluralTranslation(
 *     singular = "@count static web users",
 *     plural = "@count static web users",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\static_web\StaticWebUsersListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\static_web\StaticWebUsersAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\static_web\Form\StaticWebUsersForm",
 *       "edit" = "Drupal\static_web\Form\StaticWebUsersForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\static_web\Routing\StaticWebUsersHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "static_web_static_web_users",
 *   admin_permission = "administer static_web_static_web_users",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/static-web-users",
 *     "add-form" = "/static-web-users/add",
 *     "canonical" = "/static-web-users/{static_web_static_web_users}",
 *     "edit-form" = "/static-web-users/{static_web_static_web_users}",
 *     "delete-form" = "/static-web-users/{static_web_static_web_users}/delete",
 *     "delete-multiple-form" = "/admin/content/static-web-users/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.static_web_static_web_users.settings",
 * )
 */
final class StaticWebUsers extends ContentEntityBase implements StaticWebUsersInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the static web users was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the static web users was last edited.'));

    return $fields;
  }

}
