<?php

namespace Drupal\static_web\Service;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

/**
 * Generate Domain from cloudflare.
 */
class StaticWebDomainService {

  /**
   * {@inheritdoc}
   */
  const API_ENDPOINT = 'https://api.cloudflare.com/client/v4';

  /**
   * {@inheritdoc}
   */
  protected $data;

  /**
   * {@inheritdoc}
   */
  public function setData($data) {
    $this->data = $data;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function generateDomain() {
    $data = $this->data;
    $body = [
      'type' => 'CNAME',
      'name' => $data['site_name'],
      'content' => $data['project_domain'],
      'proxied' => TRUE,
    ];
    $data['method'] = 'POST';
    $data['body'] = json_encode($body);
    $data['path'] = '/zones/' . $data['account_id'] . '/dns_records';
    // Step 1: Generate custom subdomain in domain settings.
    $contents = $this->request($data);
    if (empty($contents)) {
      return FALSE;
    }
    $domain_name = $contents->result->name;
    $body = ['name' => $domain_name];
    $data['body'] = json_encode($body);
    $data['path'] = '/accounts/' .
      $data['zone_id'] . '/pages/projects/' .
      $data['project'] . '/domains';
    // Step 2: Register domain to custom page.
    return $this->request($data);
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectDomain() {
    $data = $this->data;
    $data['method'] = 'GET';
    $data['path'] = '/accounts/' .
      $data['zone_id'] . '/pages/projects/' .
      $data['project'];
    $response = $this->request($data);
    if ($response) {
      return $response->result->subdomain;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function request($data) {
    $headers = [
      'X-Auth-Email' => $data['email'],
      'X-Auth-Key' => $data['token'],
    ];
    $options = [];
    $options['headers'] = $headers;
    if ($data['method'] === 'POST') {
      $options['body'] = $data['body'];
    }
    try {
      $res = \Drupal::httpClient()->request($data['method'],
        self::API_ENDPOINT . $data['path'], $options);
      $contents = $res->getBody()->getContents();
      return json_decode($contents);

    }
    catch (ClientException $e) {
      \Drupal::messenger()->addError('ClientException: Cloudflare settings wrong' . $e);
      return FALSE;
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError('RequestException: Cloudflare settings wrong' . $e);
      return FALSE;
    }
    return FALSE;
  }

}
