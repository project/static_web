<?php

namespace Drupal\static_web\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\link\LinkItemInterface;
use Drupal\media\MediaInterface;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;
use Drupal\serialization\Normalizer\EntityReferenceFieldItemNormalizerTrait;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;

/**
 * Converts the Drupal entity reference item object to HAL array structure.
 */
class StaticWebGeneratorService {

  use EntityReferenceFieldItemNormalizerTrait;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = EntityReferenceItem::class;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected $componentsArray;

  /**
   * {@inheritdoc}
   */
  protected $uuid;

  /**
   * {@inheritdoc}
   */
  protected const DEFAUL_LOGO = '';

  /**
   * Constructs an StaticWebGeneratorService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface|null $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager = NULL) {
    $this->entityTypeManager = $entity_type_manager ?: \Drupal::service('entity_type.manager');
    $componentsArray = [];
  }

  /**
   * {@inheritdoc}
   */
  public function setUuid($uuid) {
    $this->uuid = $uuid;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function redirectDomainUrl($node) {
    $uid = $node->uid->target_id;
    $entity = $this->entityTypeManager->getStorage('static_web_static_web_users')
      ->loadByProperties(['field_ws_client' => $uid]);
    if (empty($entity)) {
      return FALSE;
    }
    $entity = reset($entity);
    $url = $entity->field_ws_domain_link->value;
    return 'https://' . $url;
  }

  /**
   * {@inheritdoc}
   */
  public function main() {
    $uuid = $this->uuid;
    $node = $this->getNodeEntity($uuid);
    $sections = $node->field_ws_body;
    return $sections ? $this->getSections($sections) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function sidebar() {
    $uuid = $this->uuid;
    $node = $this->getNodeEntity($uuid);
    $sections = $node->field_ws_sidebar;
    return $sections ? $this->getSections($sections) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function sidebarFlag() {
    $uuid = $this->uuid;
    $node = $this->getNodeEntity($uuid);
    return $node ? $node->field_ws_sidebar_flag->value : [];
  }

  /**
   * {@inheritdoc}
   */
  public function header() {
    $uuid = $this->uuid;
    $node = $this->getNodeEntity($uuid);
    if (empty($node)) {
      return FALSE;
    }
    $uid = $node->uid->target_id;
    $ws = $this->getWebStaticEntity($uid);
    $links = $ws->field_ws_header;
    $header_array = [];
    $link_array = [];
    foreach ($links as $link) {
      $link_array[] = [
        'title' => $link->title,
        'url' => $link->uri,
        'options' => $link->options,
      ];
    }
    $header_array = [
      'site_name' => $ws->label(),
      'links' => $link_array,
      'logo' => $ws->field_ws_site_logo->entity ?
      $ws->field_ws_site_logo->entity->uri->value :
      self::DEFAUL_LOGO,
    ];
    return $header_array;
  }

  /**
   * {@inheritdoc}
   */
  public function footer() {
    $uuid = $this->uuid;
    $node = $this->getNodeEntity($uuid);
    if (empty($node)) {
      return FALSE;
    }
    $uid = $node->uid->target_id;
    $ws = $this->getWebStaticEntity($uid);
    $links = $ws->field_ws_footer;
    $footer_array = [];
    $link_array = [];
    foreach ($links as $link) {
      $link_array[] = [
        'title' => $link->title,
        'url' => $link->uri,
        'options' => $link->options,
      ];
    }
    $footer_array = [
      'site_name' => $ws->label(),
      'links' => $link_array,
      'logo' => $ws->field_ws_site_logo->entity ?
      $ws->field_ws_site_logo->entity->uri->value :
      self::DEFAUL_LOGO,
    ];
    return $footer_array;
  }

  /**
   * {@inheritdoc}
   */
  protected function getNodeEntity() {
    $uuid = $this->uuid;
    $node = $this->entityTypeManager->getStorage('node')->loadbyProperties([
      'uuid' => $uuid,
    ]);
    if (empty($node)) {
      return FALSE;
    }
    return reset($node);
  }

  /**
   * {@inheritdoc}
   */
  protected function getWebStaticEntity($uid) {
    $ws = $this->entityTypeManager->getStorage('static_web_static_web_users')
      ->loadbyProperties(['field_ws_client' => $uid]);
    if (empty($ws)) {
      return FALSE;
    }
    return reset($ws);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSections($sections) {
    $sections_array = [];
    $paragraph = $this->entityTypeManager->getStorage('paragraph');
    foreach ($sections as $section) {
      $pid = $section->entity->id();
      $component = $paragraph->load($pid);
      $componentsArray = [];
      $componentsArray = $this->getFieldValues($component, $componentsArray);
      $sections_array[] = $componentsArray;
    }
    return $sections_array;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldValues($component, $componentsArray) {
    foreach ($component->getFields() as $machine_name => $obj) {
      if (str_contains($machine_name, 'field_') && !str_contains($machine_name, '_field_')) {
        $fieldlists = $component->$machine_name;
        // An entityreference value.
        if ($fieldlists instanceof EntityReferenceFieldItemListInterface ||
          $fieldlists instanceof FieldItemListInterface
        ) {
          foreach ($fieldlists as $field) {
            if ($component->getType() == 'breadcrumb') {
              $componentsArray[$machine_name][] = $this->getFieldItemValue($field, $machine_name);
            }
            else {
              $componentsArray[$machine_name] = $this->getFieldItemValue($field, $machine_name);
            }
          }
        }
        // A paragraph.
        if ($fieldlists instanceof EntityReferenceRevisionsFieldItemList) {
          foreach ($fieldlists as $field) {
            $componentsArray[$machine_name][] = $this->getFieldValues($field->entity, $componentsArray);
          }
        }
      }
    }
    $componentsArray['component_id'] = $component->getType();
    return $componentsArray;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldItemValue($field, $machine_name) {
    $field_array = [];
    // A Media.
    if ($field->entity instanceof MediaInterface) {
      $url = $field->entity->field_media_image->entity->uri->value;
      $alt = $field->entity->field_media_image->alt;
      $field_array = [
        'url' => $url,
        'alt' => $alt,
      ];
    }

    if ($field instanceof LinkItemInterface) {
      $url = $field->uri;
      $title = $field->title;
      $options = $field->options;
      $field_array = [
        'url' => $url,
        'title' => $title,
        'options' => $options,
      ];
    }

    if ($field instanceof StringItem) {
      $field_array = $field->value;
    }

    if ($field instanceof ListStringItem) {
      $field_array = $field->value;
    }

    if ($field instanceof TextLongItem) {
      $field_array = $field->value;
    }
    return $field_array;
  }

}
